DROP DATABASE IF EXISTS db_verifica_2;
CREATE DATABASE db_verifica_2;

USE db_verifica_2;

# Esercizio 1
CREATE TABLE clienti(
	cod_cliente					VARCHAR(3),
    nominativo					VARCHAR(10),
    
    CONSTRAINT pk_cod_cliente PRIMARY KEY(cod_cliente)
);

CREATE TABLE prodotti(
	cod_prodotto				VARCHAR(3),
    descrizione_prodotto		VARCHAR(15),
    prezzo_unitario_prodotto	DECIMAL(14,4),
    giacenza					INT,
    
    CONSTRAINT pk_cod_prodotto PRIMARY KEY(cod_prodotto)    
);

CREATE TABLE ordini(
	cod_ordine					VARCHAR(4),
    data_ordine					DATETIME,
    cod_cliente					VARCHAR(3),
    
    CONSTRAINT pk_cod_ordine PRIMARY KEY(cod_ordine),
    CONSTRAINT fk_cod_cliente_ordine FOREIGN KEY(cod_cliente) REFERENCES clienti(cod_cliente)
);

CREATE TABLE dettaglio_ordini(
	cod_dettaglio_ordini		INT AUTO_INCREMENT,
	cod_ordine					VARCHAR(4),
    cod_prodotto				VARCHAR(3),
    prezzo_vendita_prodotto		DECIMAL(14,4),
    quantity_prodotto			INT,
    
    CONSTRAINT pk_cod_dettaglio_ordini PRIMARY KEY(cod_dettaglio_ordini),
    CONSTRAINT fk_cod_ordine_ordini FOREIGN KEY(cod_ordine) REFERENCES ordini(cod_ordine),    
    CONSTRAINT fk_cod_prodotto_ordini FOREIGN KEY(cod_prodotto) REFERENCES prodotti(cod_prodotto),
    CONSTRAINT idx_cod_prodotto_ordine_ordini UNIQUE(cod_ordine, cod_prodotto)
);

CREATE TABLE caricata(
	cod_ordine					VARCHAR(4),
    data_ordine					DATETIME,
    cod_prodotto				VARCHAR(3),
    descrizione_prodotto		VARCHAR(15),
    prezzo_vendita_prodotto		DECIMAL(14,4),
    quantity_prodotto			INT,
    prezzo_unitario_prodotto	DECIMAL(14,4),
    giacenza					INT,
	cod_cliente					VARCHAR(3),
    nominativo					VARCHAR(10)
);

# Caricamento da CSV
-- LOAD DATA INFILE 'C:\\ProgramData\\MySQL\\MySQL Server 8.0\\Uploads\\verifica_01_02_2021.csv'
-- INTO TABLE caricata
-- FIELDS TERMINATED BY ";"
-- ENCLOSED BY '"'
-- LINES TERMINATED BY "\r\n"
-- IGNORE 1 ROWS;

# Caricamento con INSERT
INSERT INTO `db_verifica_2`.`caricata` (`cod_ordine`,`data_ordine`,`cod_prodotto`,`descrizione_prodotto`,`prezzo_vendita_prodotto`,`quantity_prodotto`,`prezzo_unitario_prodotto`,`giacenza`,`cod_cliente`,`nominativo`) VALUES ('OR01','2021-01-01 09:00:00','P01','PC xx',1150,1,1200,9,'C01','Rossi');
INSERT INTO `db_verifica_2`.`caricata` (`cod_ordine`,`data_ordine`,`cod_prodotto`,`descrizione_prodotto`,`prezzo_vendita_prodotto`,`quantity_prodotto`,`prezzo_unitario_prodotto`,`giacenza`,`cod_cliente`,`nominativo`) VALUES ('OR01','2021-01-01 09:00:00','P02','Stampante',300,1,300,19,'C01','Rossi');
INSERT INTO `db_verifica_2`.`caricata` (`cod_ordine`,`data_ordine`,`cod_prodotto`,`descrizione_prodotto`,`prezzo_vendita_prodotto`,`quantity_prodotto`,`prezzo_unitario_prodotto`,`giacenza`,`cod_cliente`,`nominativo`) VALUES ('OR02','2021-02-02 10:00:00','P01','PC xx',1100,2,1200,7,'C02','Verdi');
INSERT INTO `db_verifica_2`.`caricata` (`cod_ordine`,`data_ordine`,`cod_prodotto`,`descrizione_prodotto`,`prezzo_vendita_prodotto`,`quantity_prodotto`,`prezzo_unitario_prodotto`,`giacenza`,`cod_cliente`,`nominativo`) VALUES ('OR02','2021-02-02 10:00:00','P03','Tablet YY',350,1,350,3,'C02','Verdi');
INSERT INTO `db_verifica_2`.`caricata` (`cod_ordine`,`data_ordine`,`cod_prodotto`,`descrizione_prodotto`,`prezzo_vendita_prodotto`,`quantity_prodotto`,`prezzo_unitario_prodotto`,`giacenza`,`cod_cliente`,`nominativo`) VALUES ('OR03','2021-02-02 14:00:00','P03','Tablet YY',340,1,350,2,'C01','Rossi');
INSERT INTO `db_verifica_2`.`caricata` (`cod_ordine`,`data_ordine`,`cod_prodotto`,`descrizione_prodotto`,`prezzo_vendita_prodotto`,`quantity_prodotto`,`prezzo_unitario_prodotto`,`giacenza`,`cod_cliente`,`nominativo`) VALUES ('OR04','2021-02-03 11:00:00','P01','PC xx',1150,1,1200,5,'C02','Verdi');

INSERT INTO clienti
SELECT DISTINCT cod_cliente, nominativo
FROM caricata;

INSERT INTO prodotti
SELECT DISTINCT cod_prodotto, descrizione_prodotto, prezzo_unitario_prodotto, MIN(giacenza)
FROM caricata
GROUP BY cod_prodotto;

INSERT INTO ordini
SELECT DISTINCT cod_ordine, data_ordine, cod_cliente
FROM caricata;

INSERT INTO dettaglio_ordini (cod_ordine, cod_prodotto, prezzo_vendita_prodotto, quantity_prodotto)
SELECT DISTINCT cod_ordine, cod_prodotto, prezzo_vendita_prodotto, quantity_prodotto
FROM caricata;

# Esercizio 2
DELIMITER $$

CREATE TRIGGER update_giacenza AFTER INSERT ON dettaglio_ordini
FOR EACH ROW
BEGIN
	UPDATE prodotti SET prodotti.giacenza = (prodotti.giacenza - NEW.quantity_prodotto)
	WHERE prodotti.cod_prodotto = NEW.cod_prodotto;
END $$

DELIMITER ;

# Esercizio 3
SELECT o.cod_ordine, o.data_ordine, SUM(d.prezzo_vendita_prodotto*d.quantity_prodotto) AS `totale`
FROM dettaglio_ordini d INNER JOIN ordini o ON o.cod_ordine=d.cod_ordine
GROUP BY o.cod_ordine;

# Esercizio 4
SELECT c.nominativo, SUM(d.quantity_prodotto*d.prezzo_vendita_prodotto) AS `totale`
FROM dettaglio_ordini d INNER JOIN (
	SELECT c2.nominativo, o.cod_ordine
    FROM ordini o INNER JOIN clienti c2 ON c2.cod_cliente=o.cod_cliente
) AS c ON c.cod_ordine=d.cod_ordine
GROUP BY c.nominativo;

# Esercizio 5
SELECT p.cod_prodotto, p.descrizione_prodotto, SUM(d.quantity_prodotto*d.prezzo_vendita_prodotto) AS `totale`
FROM prodotti p INNER JOIN dettaglio_ordini d ON d.cod_prodotto=p.cod_prodotto
GROUP BY p.cod_prodotto;

# Esercizio 6
CREATE VIEW quantita_prodotto AS
SELECT cod_prodotto, SUM(quantity_prodotto) AS `venduti`
FROM dettaglio_ordini
GROUP BY cod_prodotto;

SELECT cod_prodotto, descrizione_prodotto
FROM prodotti
WHERE cod_prodotto IN (
	SELECT cod_prodotto
    FROM quantita_prodotto
    WHERE venduti=(SELECT MAX(venduti) FROM quantita_prodotto)
);

DROP VIEW quantita_prodotto;

# Esercizio 7
CREATE VIEW fatturato_clienti AS
SELECT o.cod_cliente, SUM(d2.quantity_prodotto*d2.prezzo_vendita_prodotto) AS `totale`
FROM ordini o INNER JOIN dettaglio_ordini d2 ON d2.cod_ordine=o.cod_ordine
GROUP BY o.cod_cliente;

SELECT c.nominativo, f.totale
FROM clienti c INNER JOIN fatturato_clienti f ON c.cod_cliente=f.cod_cliente
WHERE c.cod_cliente IN (
	SELECT cod_cliente
    FROM fatturato_clienti
    WHERE totale=(SELECT MAX(totale) FROM fatturato_clienti)
);

DROP VIEW fatturato_clienti;

# Esercizio 8
CREATE VIEW frequenza_ordini AS
SELECT DATE(o.data_ordine) AS `data`, COUNT(DISTINCT d.cod_ordine) AS `frequenza`
FROM ordini o INNER JOIN dettaglio_ordini d ON d.cod_ordine=o.cod_ordine
GROUP BY `data`;

SELECT `data`, frequenza
FROM frequenza_ordini
WHERE `data` IN (
	SELECT `data`
    FROM frequenza_ordini
    WHERE frequenza = (SELECT MAX(frequenza) FROM frequenza_ordini)
);

DROP VIEW frequenza_ordini;